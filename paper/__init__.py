VERSION = (0, 95, 0, "pre")

def get_version():
    version = '%s.%s' % (VERSION[0], VERSION[1])
    if VERSION[2]:
        version = '%s.%s' % (version, VERSION[2])
    if VERSION[3:]:
        version = '%s-%s' % (version, VERSION[3])
    return version


VERSION = (0, 95, 0, "pre")
def get_version():
    vers = [str(VERSION[0])] + [str(j) for j in VERSION[1:] if j]
    return '.'.join(vers)



@property
def kilometers(self):
    return self.__kilometers
    
@property
def km(self):
    return self.kilometers



@property
def kilometers(self):
    return self.__kilometers
km = kilometers



def __neg__(self):
    return self.__class__(-self.kilometers)
    
def __sub__(self, other):
    return self + -other


DISTANCE_UNITS = {
    'km': lambda d: d,
    'm': lambda d: units.meters(kilometers=d),
    'mi': lambda d: units.miles(kilometers=d),
    'ft': lambda d: units.feet(kilometers=d),
    'nm': lambda d: units.nautical(kilometers=d),
    'nmi': lambda d: units.nautical(kilometers=d)
}




class Location(object):
    def __init__(self, name="", point=None, attributes=None, **kwargs):
        self.name = name
        if point is not None:
            self.point = Point(point)
        if attributes is None:
            attributes = {}
        self.attributes = dict(attributes, **kwargs)




def kilometers(meters=0, miles=0, feet=0, nautical=0):
    km = 0.
    if meters:
        km += meters / 1000.
    if feet:
        miles += feet / ft(1.)
    if nautical:
        km += nautical / nm(1.)
    km += miles * 1.609344
    return km





def kilometers(meters=0, miles=0, feet=0, nautical=0):
    km = 0.
    km += meters / 1000.
    miles += feet / ft(1.)
    km += nautical / nm(1.)
    km += miles * 1.609344
    return km





def kilometers(meters=0, miles=0, feet=0, nautical=0, kilometers=0.):
    km = kilometers
    km += meters / 1000.
    miles += feet / ft(1.)
    km += nautical / nm(1.)
    km += miles * 1.609344




class Distance(object):
    def __init__(self, *args, **kwargs):
        kilometers = kwargs.pop('kilometers', 0)
        if len(args) == 1:
            # if we only get one argument we assume
            # it's a known distance instead of 
            # calculating it first
            kilometers += args[0]
        elif len(args) > 1:
            for a, b in util.pairwise(args):
                kilometers += self.measure(a, b)
       
        kilometers += units.kilometers(**kwargs)
        self.__kilometers = kilometers

def miles(kilometers=0, meters=0, feet=0, nautical=0):
    mi = 0.
    if nautical:
        kilometers += nautical / nm(1.)
    if feet:
        mi += feet / ft(1.)    # <-- bug
    if meters:
        kilometers += meters / 1000.
    mi += kilometers * 0.621371192
    return mi
