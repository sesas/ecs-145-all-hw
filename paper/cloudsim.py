#!/usr/bin/env python

'''
problem URL: http://heather.cs.ucdavis.edu/~matloff/145/Hwk/Hwk3.html

usage: python cloudsim.py cloud_size mean_interarrvl max_req mean_kv map_mid red_mid

'''

import SimPy.Simulation as s
import sys
import random as r  # http://docs.python.org/library/random.html


def rgeom(m):	# m is mean
	"""
	Generate geometrically distributed random variables
	returns an integer geometrically distributed.
	"""
	q = 1.0/m
	n = 1
	while 1:
		if r.uniform(0,1) < q: return n
		n += 1

def rgeomRoundedUp(m, nMapNodes):
	'''
	Generate rgeom using m, round up to nearest multiple of nMapNodes
	'''
	xTemp = rgeom(m)
	#print "xTemp = ", xTemp
	if not xTemp % nMapNodes: 
		return xTemp
	else: 
		xTemp += nMapNodes
		mult = xTemp/nMapNodes
		return nMapNodes * mult
	#n = xTemp + nMapNodes % xTemp 
	

def runif(a, b):
	'''
	Generate uniformly distributed random variables
	r.uniform(a,b) test
	Return a random floating point number N such that 
	a <= N <= b for a <= b and b <= N <= a for b < a.'''
	return r.uniform(a, b)

def rexp(mean):
	"""
	Generate exponentially distributed random variables
	r.expovariate(lambd)
	Exponential distribution. lambd is 1.0 divided by the desired mean. 
	It should be nonzero."""
	return r.expovariate(1/mean)


class G:
	"""
	Global class
	"""
	debug = 0
	cloud_size = None
	mean_interarrvl = None
	max_req = None 
	mean_kv = None
	map_mid = None
	red_mid = None
	
	totJobs = 0
	reqNodes = 0

	cloudNodes = None
	reduceNodes = None
	mapNodes = None
	
	def __init__(self, cmdLineArgvs):
		cmdLineArgvs = [eval(j) for j in cmdLineArgvs]
		G.cloud_size, G.mean_interarrvl, G.max_req, G.mean_kv, G.map_mid, G.red_mid = cmdLineArgvs
		G.cloudNodes = s.Resource(G.cloud_size)
		
class bookkeeping:	
	'''
	Book keeping class for statistical data
	has __str__ function
	'''
	userArrivals = []
	numberNodesRequested = []
	usersRejected = [0]
	usersAccepted = [0]
	numberJobsSpawned = []
	mapProcessingTimes = []
	redProcessingTimes = []
	waitTimeForMapPhase = []
	waitTimeForRedPhase = []
	totalPairCompletionTime = []
	busyNodes = []
	
	@classmethod 
	def clear(self):
		bk.userArrivals = []
		bk.numberNodesRequested = []
		bk.usersRejected = [0]
		bk.usersAccepted = [0]
		bk.numberJobsSpawned = []
		bk.mapProcessingTimes = []
		bk.redProcessingTimes = []
		bk.waitTimeForMapPhase = []
		bk.waitTimeForRedPhase = []
		bk.totalPairCompletionTime = []
		bk.busyNodes = []
	
	def __str__(self):
		rR = len(bookkeeping.usersRejected) / float( len(bookkeeping.usersRejected) + len(bookkeeping.usersAccepted))
		bN = average(bk.busyNodes)
		uA = average(bookkeeping.userArrivals)
		nNR = average(bookkeeping.numberNodesRequested) 
		nJS = average(bookkeeping.numberJobsSpawned)
		mPT = average(bookkeeping.mapProcessingTimes)
		rPT = average(bookkeeping.redProcessingTimes)
		MP = average(bookkeeping.waitTimeForMapPhase)
		RP = average(bookkeeping.waitTimeForRedPhase)
			
		out = [
			'Rejected ratio:\t\t%5.2f'%rR, 	
			'Mean busy nodes:\t%5.2f'%bN, 
			'User arrivals:\t\t%5.2f'%uA,
			'N nodes requested:\t%5.2f'%nNR, 
			'N jobs spawned:\t\t%5.2f'%nJS,
			'Map processing times:\t%5.2f'%mPT,
			'Red processing times:\t%5.2f'%rPT,
			'Map phase:\t\t%5.2f'%MP,
			'Red phase:\t\t%5.2f'%RP
			]
		return str(reduce(lambda x,y: x+'\n'+y,out))
		
bk = bookkeeping
gl = None

def average(inList):
	'''
	Gets average of given list
	'''
	try:
		return sum(inList) / float( len(inList))
	except:
		return 0
	
class theCloud:
	'''
	Global Cloud class for node resources
	'''
	cloudResources = None
	
	def __init__(self):
		theCloud.cloudResources = [ s.Resource(1) for j in range(G.cloud_size) ]
	
	@classmethod
	def countAvailableNodes(self):
		availList = [ res.n for res in theCloud.cloudResources]
		return sum(availList)
		
	@classmethod
	def grabNnodes(self, N):
		availIndexes = filter( lambda x: x.n, theCloud.cloudResources )
		#availIndexes = [ ind if res.n else None for ind, res in enumerate(theCloud.cloudResources) ]
		#availIndexes = filter(None, availIndexes)
		return availIndexes[:N]
		
class userArrivals(s.Process):
	'''
	Subclass of Process that schedules user job arrival events
	'''
	def __init__(self):
		s.Process.__init__(self)
		pass
	
	def Run(self):
		while 1:
			userArrivalTime = rexp(G.mean_interarrvl)
			bookkeeping.userArrivals.append(userArrivalTime)
			yield s.hold, self, userArrivalTime	
			# user job arrives
			
			nodesRequest = r.randint(2, G.max_req)
			bk.numberNodesRequested.append(nodesRequest)
			
			if theCloud.countAvailableNodes() < nodesRequest:	# if not enough free nodes
				# the user job ibk.s rejected
				bk.usersRejected.append( s.now() - bk.usersRejected[-1] )
			else:	# other wise process job
				# the user is accepted
				bk.usersAccepted.append( s.now() - bk.usersAccepted[-1] )
				
				# spawn 1 new user job with nodesRequest nodes (as resource)
				newUserJob = userJob( theCloud.grabNnodes( nodesRequest ) )
				s.activate( newUserJob, newUserJob.Run() )
				
class userJob(s.Process):
	'''
	Subclass of Process that simulates accepted user jobs
	'''
	def __init__(self, resourcesAvailable):
		s.Process.__init__(self)
		self.reduceNode = resourcesAvailable[-1]
		self.mapNodes = resourcesAvailable[:-1]
		self.userJob_Pairs = []
		
	def Run(self):
		# determine how many pairs are to be spawned
		nPairs = rgeomRoundedUp(G.mean_kv, len(self.mapNodes))
		bookkeeping.numberJobsSpawned.append(nPairs)
		
		# instantiate each pair with their own resources
		for j in range(nPairs):
			dedicated_map = self.mapNodes[ j % len(self.mapNodes) ]
			newPair = Pair( dedicated_map , self.reduceNode, self )
			s.activate( newPair, newPair.Run() )
			self.userJob_Pairs.append(newPair)
			
		# sleep until all pairs are done processing
		while self.userJob_Pairs:
			yield s.passivate, self
			
		# calculate how many nodes are busy at this time
		bk.busyNodes.append( len(theCloud.cloudResources) - theCloud.countAvailableNodes() )
			
class Pair(s.Process):
	'''
	Subclass of Process that simulates Mapping and Reducing of each pair
	'''
	def __init__(self, mapnode, rednode, userJob):
		s.Process.__init__(self)
		self.mapnode = mapnode
		self.rednode = rednode
		self.userJob = userJob  # what user job is the owner of this pair?
		
	def Run(self):
		init_time = s.now()
		# request map node
		yield s.request, self, self.mapnode
		bk.waitTimeForMapPhase.append( s.now() - init_time )
		
		# determine map processing time
		mapCompletionTime = runif(0.5 * G.map_mid, 1.5 * G.map_mid)
		bookkeeping.mapProcessingTimes.append(mapCompletionTime)
		
		yield s.hold, self, mapCompletionTime 
		yield s.release, self, self.mapnode		
		# mapping is done, release this node
		
		# queueing into reducing		
		time_before_reduce = s.now()
		yield s.request, self, self.rednode
		bk.waitTimeForRedPhase.append( s.now() - time_before_reduce )
		
		redCompletionTime = runif(0.5 * G.red_mid, 1.5 * G.red_mid)
		bookkeeping.redProcessingTimes.append(redCompletionTime)
		
		yield s.hold, self, redCompletionTime 
		yield s.release, self, self.rednode		
		# reducing is done, release this nodes
		
		bk.totalPairCompletionTime.append( s.now() - init_time )
		
		self.userJob.userJob_Pairs.remove(self)
		s.reactivate(self.userJob)	# after all pairs are done, wake up userJob process
		

def testThis():
	'''
	Professor Matloff's test cases
	'''
	testCases = '''	100	2	2.0	2.0	0.1	0.1	0.00	0.20
			100	2	2.0	2.0	0.1	10.0	0.00	9.84
			2	2	2.0	2.0	0.1	10.0	0.90	0.00
			4	2	2.0	2.0	0.1	10.0	0.71	1.79
			20	5	4.0	7.0	10.0	10.0	0.47	15.82
			2	2	5.0	1.0	0.01	0.01	2.23	23.23'''
	testCases = [test.split('\t') for test in testCases.split('\n')]

	testCases = [[test[0], test[2], test[1]] + test[3:-2] for test in testCases]
#	testCT = []
	
#	for test in testCases:
#		testCT.append( [eval(j) for j in test] )
#	testCases = testCT
	print testCases
	
	for test in testCases[:]:
		gl = G(test)
		cloud = theCloud()
		s.initialize()
		UA = userArrivals()
		s.activate(UA, UA.Run())
		maxSimTime = float(1000)
		s.simulate(until=maxSimTime)
		print test
		#print UA.rejectedUsers, UA.TotalUsers, UA.rejectedUsers / float( UA.TotalUsers )
		print bk()
		bk.clear()
		#print UA.rejectedUsers, UA.TotalUsers, UA.rejectedUsers / float( UA.TotalUsers ), G.totJobs, float(G.totJobs/UA.TotalUsers), G.reqNodes, float(G.reqNodes/UA.TotalUsers)
	

def main():
	'''
	Simulation is the following:
	userArrivals schedules arrivals of user jobs. 
	From its Run(), it allocates requested nodes and spawns a userJob process if requested nodes are available.
	userJob process calculates how many pairs to distribute amont nodes.
	From its Run(), it spawns Pair processes and sleeps until all the Pair are mapped and reduced
	Pair process requests degsignated map node resource and gets "processed"
	After it releases map node, it requests reduce node and gets "processed"
	After that it releases the reduce node and reactive its parent userJob process
	'''
	global gl, testCases	# global variables
	try:
		#print sys.argv
		gl = G(sys.argv[1:])
		stdArgv = sys.argv[1:]
	except: 
		if G.debug:
			testThis()
			return
		else:
		#print sys.argv
#		raise
			stdArgv = (100, 2.0, 2, 2.0, .1, .1) # G.cloud_size, G.mean_interarrvl, G.max_req, G.mean_kv, G.map_mid, G.red_mid
			stdArgv = [ str(j) for j in stdArgv ] 
			print "It doesn't look like you are using the right arguments."
			print "Using the default arguments for now:", stdArgv
			gl = G(stdArgv)
	cloud = theCloud()		# theCloud object with resource nodes
	s.initialize()			# initialize simulator
	UA = userArrivals()		# userArrivals process object. schedule user job arrivals
	s.activate(UA, UA.Run())	
	maxSimTime = 10000.0
	s.simulate(until=maxSimTime)
	# printing the results
	print '+----------+\n|  \033[1;31mInput\033[1;m   |\n+----------+' # Radish red
	print '\033[1;36mSize\033[1;m\t\033[1;36mArrv\033[1;m\t\033[1;36mMaxreq\033[1;m\t\033[1;36mmeankv\033[1;m\t\033[1;36mMapmid\033[1;m\t\033[1;36mRedmid\033[1;m'
	print reduce(lambda x,y: x+'\t'+y,stdArgv)
	print '+----------+\n|  \033[1;32mOutput\033[1;m  |\n+----------+' # Grass green
	print bk()
	print 
	print '\033[1;41m NOTE: \033[1;m Professor Matloff\'s \033[1;36mmaxreq\033[1;m and \033[1;36marrv\033[1;m columns positions'
	print 'in HW3 specs are swapped compared to the order in \'Usage\'' 
	bk.clear()
		
	
		
if __name__ == "__main__":
	main()
	
		
	
