# strclnt.py: Client code for auction program
#
# Each client will run this program, connecting it to a certain server.  
# 
# this is the client; usage is
# python strclnt.py server_address server_port_number


import socket
import sys
import select # eyan
import server_curses

try:
	server_curses.init()
	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	host = sys.argv[1] # server address
	port = int(sys.argv[2]) # server port

	server.connect((host, port))	# eyan

	select_inputs = [server, sys.stdin]
	client_input_string = ''	# initialize the input to be null

	server_curses.addToAnnouncements("Your client is connected to host:")
	server_curses.addToAnnouncements(str(server.getpeername()))

except:
	server_curses.restorescreen()
	print "Failed to start"
	server.close() # close socket
	sys.exit()

while(1):
	try:
		# 1. listen for either client typing in something OR
		# 2. the server sent us some information back
		inputready,outputready,exceptready = select.select(select_inputs,[],[])

		for s in inputready:

			if s == server:
				# the server is sending us information
				'''
				print "1"
				print "Client just recieved:"
				print s.getpeername() # this is the server's port if looking from the my client side
				print s.getsockname()	# this is MY port if looking from the server side
				'''

				# read in the packing that the server is sending to us
				incoming_packet = s.recv(1024)

				# Parse the incoming packet string.  It is of the form:
				# 0 announcement texts
				# 1 notification of another bid that has taken place
				# 2 response to a bid that this client sent

				# grab the first character from the packet
				try:
					action_bit = int(incoming_packet[0:1])
					if action_bit == 0:
						server_curses.addToAnnouncements("Announcement: " + incoming_packet[2:])
					elif action_bit == 1:
						server_curses.addToBids("New Bid: " + incoming_packet[2:])
					elif action_bit == 2:
						server_curses.addToAnnouncements("Response: " + incoming_packet[2:], 1)
					elif action_bit == 3:
						server_curses.addToBids("New Bid: " + incoming_packet[2:], 1)
					else:
						pass
				except:
					pass
		



			elif s == sys.stdin:
				# User is trying to enter a bid

				# each keypress/mouse click is an "action". We need to call the curses
				# function so that we can echo is onto the current screen

				# getInput() returns me the integer ascii value of the keystroke
				command = server_curses.getInput()

				# ESC key or "quit" command
				if command == -1 or str(command).lower() == "quit":
					server_curses.restorescreen()
					s.close() # close socket
					sys.exit()
				elif command != 0:
					# we pressed "enter", validate the command
					# command will be of format "## ##.##"
					token = command.split()
					try:
						itemID, bid = [eval(j) for j in token]	
						itemID, bid = [int(token[0]), float(token[1])]						
						server.sendto(command, server.getpeername())
					except:
						server_curses.addToAnnouncements("Invalid Input: '" + command + "'", 1)
					'''
					print "Client sending to server:"
					print "raw str:" + repr(bid)
					print server.getpeername()
					'''
			else:
				# handle all other sockets
				data = s.recv(1024)
				# print "3 - WTF, HOW DID I GET HERE?" # LMAO
				# print data
	except KeyboardInterrupt:
		server_curses.restorescreen()
		server.close() # close socket
		sys.exit()

server.close() # close socket
print test
