#/usr/bin/env python

#http://stackoverflow.com/questions/2020476/how-do-i-send-a-mail-via-mailx-subprcoess
import random
import subprocess
import sys

#recipientsList = '''kenickles@ucdavis.edu
#eyan@ucdavis.edu
#ddnnyugen@ucdavis.edu
#jksoe@ucdavis.edu
#gabreyla@gmail.com'''.split()
#print recipientsList
recipient = 'gabreyla@gmail.com'
subjectTxt = '"The Auction has Closed: Your invoice"'
bodyTxt = '"your total bids was: 2000"'
#fullCommand = ['echo', bodyTxt, '| mail -s', subjectTxt, ' '.join(recipientsList)]
#cmd = ' '.join(fullCommand)
##cmd="""echo "test" | mailx -s 'test!' """ + ' '.join(recipients)
#p=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
#output, errors = p.communicate()
#print errors,output

def send1Email(recipient, bodyTxt, subjectTxt = 'The Auction has Closed: Your invoice is ready'):
    recipientsList = [recipient]
    bodyTxt = '"'+bodyTxt+'"'
    subjectTxt = '"'+subjectTxt+'"'
    fullCommand = ['echo', bodyTxt, '| mail -s', subjectTxt, ' '.join(recipientsList)]
    cmd = ' '.join(fullCommand)
    #cmd="""echo "test" | mailx -s 'test!' """ + ' '.join(recipients)
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    output, errors = p.communicate()
    return errors,output

subjectTemplate = "The Auction is Over. Congratulations, you won!"
bodyTemplate = """Congratulations XXXXX,

You won item YYYYY. The amount you bidded was ZZZZZ.

Please make that amount payable to the auctioneer. 

Thank you for participating in the auction.
"""

def sendEmailsToWinners(itemsList, mapCoonToEmails):
	for item in itemsList:
                if not item.CurrentWinner: continue
		email = mapCoonToEmails[item.CurrentWinner]
		try:
			body = bodyTemplate.replace('XXXXX', email).replace('YYYYY', str(item.Number)).replace('ZZZZZ', str(item.CurrentAmount))
		except:
			print [repr(j) for j in [email, item.Number, item.CurrentAmount]]
			print itemsList
			print mapCoonToEmails
			raise
		subject = subjectTemplate
		send1Email(email, body, subject)
		
		

if __name__ == '__main__':
    # for testing only, from http://en.wikipedia.org/wiki/United_States_Army_Materiel_Command
    randomWords = '''The U.S. Army Materiel Command (AMC) is the primary provider of materiel to the United States Army. The Command's mission includes the research & development of weapons systems as well as maintenance and parts distribution.
    AMC operates research and development engineering centers; Army Research Laboratories; depots; arsenals; ammunition plants; and other facilities, and maintains the Army?s prepositioned stocks, both on land and afloat. The command is also the Department of Defense Executive Agent for the chemical weapons stockpile and for conventional ammunition.
    AMC is responsible within the United States Department of Defense for the business of selling Army equipment and services to allies of the United States and negotiates and implements agreements for co-production of U.S. weapons systems by foreign nations.
    AMC is currently headquartered at Redstone Arsenal, AL, and is located in approximately 149 locations worldwide, including more than 49 American States and 50 countries. AMC maintains employment of upwards of 70,000 military and civilian employees.
    The U.S. Army Materiel Command was established on 8 May 1962 and was activated on 1 August of that year as a major field command of the U.S. Army. Lieutenant General Frank S. Besson, Jr., who directed the implementation of the Department of Army study that recommended creation of a "materiel development and logistics command", served as its first commander.
    The 2005 Base Realignment and Closure decision relocated AMC to Redstone Arsenal, Ala. Personnel began relocating to Redstone in 2006 and the command will be completely relocated by summer 2011.'''

    
    wordList = randomWords.split()
    randomBody = random.sample(wordList, random.randint(1, len(wordList)))
    send1Email(recipient, ' '.join(randomBody))
    
    #test the sendEmailsToWinners function
    with open('bidder_file') as f:
        emails = f.readlines()
    emails = [j.strip() for j in emails]
    import aucsrvr
    r = random
    keys = [r.randint(0, 2000) for j in emails]
    items = [aucsrvr.Item(r.randint(0, 1024), key, r.randint(0, 99999)) for key in keys]
    mapCoonToEmails = {}
    mapCoonToEmails.update( zip(keys, emails) )
    sendEmailsToWinners(items, mapCoonToEmails)

#http://docs.python.org/library/email-examples.html
## Import smtplib for the actual sending function
#import smtplib

## Import the email modules we'll need
#from email.mime.text import MIMEText

#sender = 'gabreyla@gmail.com'
#recipients = ['gabreyla@gmail.com']
#textfile = 'email.txt'

## Open a plain text file for reading.  For this example, assume that
## the text file contains only ASCII characters.
#fp = open(textfile, 'rb')
## Create a text/plain message
#msg = MIMEText(fp.read())
#fp.close()

## me == the sender's email address
## you == the recipient's email address
#msg['Subject'] = 'The contents of %s' % textfile
#msg['From'] = sender
#msg['To'] = recipients

## Send the message via our own SMTP server, but don't include the
## envelope header.
#s = smtplib.SMTP('localhost')
#s.set_debuglevel(3)
#s.sendmail(me, you, msg.as_string())
#s.quit()



## http://stackoverflow.com/questions/2020476/how-do-i-send-a-mail-via-mailx-subprcoess
#import smtplib
## email options
#SERVER = "localhost"
#FROM = "gabreyla@gmail.com"
#TO = ["gabreyla@gmail.com"]
#SUBJECT = "Alert!"
#TEXT = "This message was sent with Python's smtplib."


#message = """\
#From: %s
#To: %s
#Subject: %s

#%s
#""" % (FROM, ", ".join(TO), SUBJECT, TEXT)

#server = smtplib.SMTP(SERVER)
#server.set_debuglevel(3)
#server.sendmail(FROM, TO, message)
#server.quit()
