# strsvr.py: simple illustration of nonblocking sockets
# multiple clients connect to server; each client repeatedly sends a
# letter k, which the server adds to a global string v and echos back
# to the client; k = '' means the client is dropping out; when all
# clients are gone, server prints final value of v

# this is the server; usage is
# 	python strsvr.py server_port_number

import socket, sys
# set up listening socket
lstn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)



port = int(sys.argv[1])
# bind lstn socket to this port
lstn.bind(('', port))
lstn.listen(5)

# initialize concatenate string, v
v = ''
curr_bid = 0

# initialize client socket list
cs = []

# in this example, a fixed number of clients, nc
nc = 2

# accept connections from the clients
for i in range(nc):
	(clnt,ap) = lstn.accept()
	# set this new socket to nonblocking mode
	clnt.setblocking(0)
	cs.append(clnt)
	print "This client just connected to you:"
	print clnt.getpeername()

# now loop, always accepting input from whoever is ready, if any, until
# no clients are left
while (len(cs) > 0):
	# get next client, with effect of a circular queue
	clnt = cs.pop(0)
	cs.append(clnt)
	# something ready to read from clnt? clnt closed connection?
	try:
		k = clnt.recv(1024)	# try to receive one byte; if none is ready
											# yet, that is the "exception"
		print "Server just got: " + repr(str(k))
		if k == '':
			clnt.close()
			cs.remove(clnt)

		token = k.split()
		if curr_bid < token[1]:
			curr_bid = token[1]
		else:
			clnt.sendto("2 Your bid of '" + str(token[1]) + "' is lower than the current bid '" + str(curr_bid) + "'", clnt.getpeername())
			continue

		# send back to the client who sent this to the server, one to one
		# this gets repeated in for() loop below
		
		# print "Sending to1:"
		# print str(k)
		# clnt.sendto(str(k), clnt.getpeername())
		

		# eyan - send the string to ALL current clients except the one who sent it
		for eyan_clnt in cs:
			if eyan_clnt != clnt:
				#print "Sending to2:"
				#print eyan_clnt.getpeername()
				eyan_clnt.sendto("1 Someone bidded: " + str(curr_bid), eyan_clnt.getpeername())
			else:
				eyan_clnt.sendto("3 Your bid of " + str(curr_bid) + " is good!", eyan_clnt.getpeername())
		# end eyan
	except: pass

lstn.close()
print 'the final value of v is', v

