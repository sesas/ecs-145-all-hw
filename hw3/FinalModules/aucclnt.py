# strclnt.py: Client code for auction program
#
# Each client will run this program, connecting it to a certain server.  
# 
# this is the client; usage is
# python aucclnt.py server_address server_port_number


import socket
import sys
import select # eyan
import server_curses

try:
	server_curses.init()
	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	host = sys.argv[1] # server address
	port = int(sys.argv[2]) # server port

	server.connect((host, port))	# eyan

	flo_r = server.makefile('r',0)	# file descriptor for makefile, used for reading data sent by the server
	flo_w = server.makefile('w',0)	# file descriptor for makefile, used for writing(sending) data back to the server
	
	select_inputs = [flo_r, sys.stdin]
	client_input_string = ''	# initialize the input to be null

	server_curses.addToAnnouncements("Your client is connected to host:")
	server_curses.addToAnnouncements(str(server.getpeername()))
	
	possibleCommands = ['"item# price" to bid on an item', 
						'"mess: message-content" to send a message to the auctioneer',   
						'"status" to request the status of the current item',   
						'quit',
						'or you can press CTRL+C to quit as well.',
					]
	server_curses.addToAnnouncements("The possible bidder's commands are:")
	for command in possibleCommands:
		server_curses.addToAnnouncements('- ' + command)
	
except:
	server_curses.restorescreen()
	print "Failed to start"
	server.close() # close socket
	sys.exit()

while(1):
	try:
		# 1. listen for either client typing in something OR
		# 2. the server sent us some information back
		inputready,outputready,exceptready = select.select(select_inputs,[],[])

		for s in inputready:

			if s == flo_r:
				# the server is sending us information
				'''
				print "1"
				print "Client just recieved:"
				print s.getpeername() # this is the server's port if looking from the my client side
				print s.getsockname()	# this is MY port if looking from the server side
				'''

				# read in the packing that the server is sending to us
				incoming_packet = s.readline()

				# Parse the incoming packet string.  It is of the form:
				# 0 announcement texts
				# 1 notification of another bid that has taken place
				# 2 response to a invalid bid that this client sent
				# 3 responese to a valid higher bid
				# 4 close command sent from server

				# grab the first character from the packet
				try:
					action_bit = int(incoming_packet[0:1])
				except:
					pass
				if action_bit == 0:
					server_curses.addToAnnouncements("Announcement: " + incoming_packet[2:])
				elif action_bit == 1:
					server_curses.addToBids("New Bid: " + incoming_packet[2:])
				elif action_bit == 2:
					server_curses.addToAnnouncements("Response: " + incoming_packet[2:], 2)
				elif action_bit == 3:
					server_curses.addToBids("New Bid: " + incoming_packet[2:], 3)
				elif action_bit == 4:
					server_curses.restorescreen()
					s.close() # close socket
					print '''
The Auction has closed. 
If you won a bidding, you will be notified by email how to proceed from here.

Thank you, 
Have a good day.
'''
					sys.exit()
				else:
					pass

			elif s == sys.stdin:
				# User is trying to enter a bid

				# each keypress/mouse click is an "action". We need to call the curses
				# function so that we can echo is onto the current screen

				# getInput() returns me the integer ascii value of the keystroke
				command = server_curses.getInput()

				# ESC key or "quit" command
				if command == -1 or str(command).lower() == "quit":
					server_curses.restorescreen()
					s.close() # close socket
					sys.exit()
				elif command != 0:
					if '@' in command or command.startswith('mess:') or command.lower().strip() in 'status':
						flo_w.write(command + "\n")
						continue

					# we pressed "enter", validate the command
					# command will be of format "## ##.##"
					token = command.split()
					try:
						itemID, bid = [eval(j, {}) for j in token]	
						#itemID, bid = [int(token[0]), float(token[1])]						
						flo_w.write(command + "\n")
					except:
						server_curses.addToAnnouncements("Invalid Input: '" + command + "'", 1)
					'''
					print "Client sending to server:"
					print "raw str:" + repr(bid)
					print server.getpeername()
					'''
	except KeyboardInterrupt:
		server_curses.restorescreen()
		server.close() # close socket
		sys.exit()
	#finally:
		#server_curses.restorescreen()
		#server.close() # close socket
		#sys.exit()

server.close() # close socket
print test
