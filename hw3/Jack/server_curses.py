import curses, os, sys, traceback, string

"""
global variables for CURSES
"""
class gb:
	# HW3 
	cscrn = None	# Current screen/pad
	pscrn = None	# Parent screen
	bpad = None	# Bids pad
	apad = None	# Annce pad
	ipad = None	# Input pad
	
	inp = ""	# input string
	
	padsMax = {'bpad':[], 'apad':[], 'ipad':[]}	# Dictionary to keep track of maxyx
							# for pads		
			
	prevHighlight = []  # stores record of previously highlighted

	mouse = ()	# device_id, mouse_x, mouse_y, mouse_z, eventType	
	
	SWmaxyx = []	 #max dimension for SW Window
	IWmaxyx = []	 #max dimension for IW Window
	
	maxys = None


"""
Screen object to help deal with pads easier - refresh(...)
"""
class screen:
	def __init__(self, scrn):
		self.scrn = scrn

		# pad.refresh(pminrow,pmincol,sminrow,smincol,smaxrow,smaxcol)
		self.pminrow = 0
		self.pmincol = 0
		self.sminrow = 0
		self.smaxrow = 0
		self.smincol = 0
		self.smaxcol = 0
		# pad.refresh(pminrow,pmincol,sminrow,smincol,smaxrow,smaxcol)

		# current row and col for pads. for scrolling purpose
		self.currow = 0
		self.curcol = 0
		self.maxyx = scrn.getmaxyx()
	
	# Refresh helper function to refresh pads
	def refresh(self):
		if self.scrn != None:
			self.scrn.refresh(self.pminrow, self.pmincol, self.sminrow, self.smincol, self.smaxrow, self.smaxcol)
'''
# Move to the other sub window
def movetoothersubwin():
        if gb.curscrn == gb.SWscrn:
                gb.curscrn = gb.IWscrn
        elif gb.curscrn == gb.IWscrn:
		gb.curscrn = gb.SWscrn
        gb.curscrn.refresh()	# REFRESH SETS "ACTIVE" WINDOW

# Move one line up or down and scroll if necessary 
def moveonelineupdown(inc):
	curyx = gb.curscrn.scrn.getyx()
	parentmaxyx = gb.parent.maxyx
	curmaxyx = gb.curscrn.maxyx
	oldPage = gb.pdfObj.whatpage(curyx[0])
	newPage = gb.pdfObj.whatpage(curyx[0]+inc)


	if(inc == 1):		
		if(gb.curscrn.currow < (gb.curscrn.smaxrow - gb.curscrn.sminrow)):
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
			gb.curscrn.currow += inc
		elif(curyx[0] < curmaxyx[0] - 1):
			gb.curscrn.pminrow += inc
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])		
	else:
		if(gb.curscrn.currow > 0):
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
			gb.curscrn.currow += inc
		elif(curyx[0] > 0):
			gb.curscrn.pminrow += inc
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
	gb.curscrn.refresh()
'''

def init(isServer = 0):
	# window setup
	gb.cscrn = screen(curses.initscr())


	gb.pscrn = gb.cscrn	# parent screen pointer
	#curses.noecho()
	# curses.cbreak()
	curses.def_prog_mode()
	

	
	# getmaxyx return a tuple (y, x) of h and w of the window
	gb.maxyx = gb.cscrn.scrn.getmaxyx()
	if(gb.maxyx[0] < 24 and gb.maxyx[1] < 80):
		print "Minimum terminal size: 80 x 24"
		return
		
	half = gb.maxyx[0]/2, gb.maxyx[1]/2	# To split screen halfway

	#intialize pad screens
	gb.apad = screen(curses.newpad(gb.maxyx[0] +5, half[1]))
	gb.bpad = screen(curses.newpad(gb.maxyx[0] * 2, half[1]))
	gb.ipad = screen(curses.newpad(half[0], half[1]))

	
	#allow keypad for mouse
	gb.apad.scrn.keypad(1)
	gb.bpad.scrn.keypad(1)
	gb.ipad.scrn.keypad(1)

	#define edges for apad
	gb.apad.sminrow = 1
	gb.apad.smaxrow = gb.maxyx[0]-1
	gb.apad.smincol = half[1]
	gb.apad.smaxcol = gb.maxyx[1]-1

	#define edges for bpad
	gb.bpad.sminrow = 1
	gb.bpad.smaxrow = half[0] - 1
	gb.bpad.smincol = 0
	gb.bpad.smaxcol = half[1]

	#define edges for ipad
	gb.ipad.sminrow = half[0]+1
	gb.ipad.smaxrow = gb.maxyx[0]-1
	gb.ipad.smincol = 0
	gb.ipad.smaxcol = half[1]-1

	#gb.curscrn = gb.IWscrn

	#set labels from parent screen
	gb.pscrn.scrn.addstr(0,0, " === Bids === ", curses.A_REVERSE)
	if isServer:
		gb.pscrn.scrn.addstr(half[0],0, " === Auctioneer's Input === ", curses.A_REVERSE)
	else:
		gb.pscrn.scrn.addstr(half[0],0, " === Bidder's Input === ", curses.A_REVERSE)
	gb.pscrn.scrn.addstr(0,half[1], " === Announcements === ", curses.A_REVERSE)
	gb.pscrn.scrn.refresh()
	
	#set intial cursor
	gb.apad.scrn.move(0,0)
	gb.apad.refresh()
	gb.bpad.scrn.move(0,0)
	gb.bpad.refresh()
	gb.ipad.scrn.move(0,0)
	gb.ipad.refresh()
	#curses.mousemask(curses.ALL_MOUSE_EVENTS)
	gb.cscrn = gb.ipad
	gb.cscrn.refresh()
	


def addToScrn(cscrn, string, isHighlighted = 0):
	padyx = cscrn.scrn.getyx()
	inc = len(string)/(cscrn.smaxcol-cscrn.smincol)+1
	if isHighlighted:
		cscrn.scrn.addstr(padyx[0], 0, string, curses.A_REVERSE)
	else:
		cscrn.scrn.addstr(padyx[0], 0, string)

	if (padyx[0] + inc) >= (cscrn.maxyx[0] -1):
		for i in range(inc):
			cscrn.scrn.move(0,0)
			cscrn.refresh()
			cscrn.scrn.deleteln()
			cscrn.refresh()
		padyx = [padyx[0]-inc, padyx[1]]

	if padyx[0] - cscrn.pminrow + inc > cscrn.smaxrow:
		cscrn.pminrow = padyx[0] + inc - cscrn.smaxrow
	cscrn.scrn.move(padyx[0]+inc, 0)
	cscrn.refresh()
	gb.ipad.refresh()

def addToAnnouncements(string, isError = 0):
	addToScrn(gb.apad, string, isError)

def addToBids(string, isYours = 0):
	addToScrn(gb.bpad, string, isYours)

	
def getInput():
    #try:
        ch = gb.cscrn.scrn.getch()
        if ch == 27:
            return -1
        if ch != 10:		# 10 is ASCII rep of RETURN key
            if  ch > 31 and ch < 128:
                gb.inp += chr(ch)
            if ch == curses.KEY_BACKSPACE:
                gb.inp = gb.inp[:-1]
                gb.cscrn.scrn.clear()
                gb.cscrn.scrn.addstr(0,0,gb.inp)
                gb.cscrn.refresh()
            elif ch > 31 and ch < 128:
                gb.cscrn.scrn.echochar(ch)
        elif ch == 10:
            gb.cscrn.scrn.clear()
            gb.cscrn.refresh()
            temp = gb.inp
            gb.inp = ""
            return temp
        return 0
    #except KeyboardInterrupt:
        #restorescreen()
        #return -1
	
# Main window object
"""
Main function for curses
"""
def main():	
	# window setup
	gb.cscrn = screen(curses.initscr())


	gb.pscrn = gb.cscrn	# parent screen pointer
	#curses.noecho()
	# curses.cbreak()
	curses.def_prog_mode()
	

	
	# getmaxyx return a tuple (y, x) of h and w of the window
	maxyx = gb.cscrn.scrn.getmaxyx()
	if(maxyx[0] < 24 and maxyx[1] < 80):
		print "Minimum terminal size: 80 x 24"
		return
		
	half = maxyx[0]/2, maxyx[1]/2	# To split screen halfway

	#intialize pad screens
	gb.apad = screen(curses.newpad(maxyx[0] * 2, half[1]))
	gb.bpad = screen(curses.newpad(maxyx[0] * 2, half[1]))
	gb.ipad = screen(curses.newpad(half[0], half[1]))

	
	#allow keypad for mouse
	gb.apad.scrn.keypad(1)
	gb.bpad.scrn.keypad(1)
	gb.ipad.scrn.keypad(1)

	#define edges for apad
	gb.apad.sminrow = 1
	gb.apad.smaxrow = maxyx[0]-1
	gb.apad.smincol = half[1]
	gb.apad.smaxcol = maxyx[1]-1

	#define edges for bpad
	gb.bpad.sminrow = 1
	gb.bpad.smaxrow = half[0] - 1
	gb.bpad.smincol = 0
	gb.bpad.smaxcol = half[1]

	#define edges for ipad
	gb.ipad.sminrow = half[0]+1
	gb.ipad.smaxrow = maxyx[0]-1
	gb.ipad.smincol = 0
	gb.ipad.smaxcol = half[1]-1

	#gb.curscrn = gb.IWscrn

	#set labels from parent screen
	gb.pscrn.scrn.addstr(0,0, " === Bids === ", curses.A_REVERSE)
	gb.pscrn.scrn.addstr(half[0],0, " === Auctioneer's Input === ", curses.A_REVERSE)
	gb.pscrn.scrn.addstr(0,half[1], " === Announcements === ", curses.A_REVERSE)
	gb.pscrn.scrn.refresh()
	
	#set intial cursor
	gb.apad.scrn.move(0,0)
	gb.apad.refresh()
	gb.bpad.scrn.move(0,0)
	gb.bpad.refresh()
	gb.ipad.scrn.move(0,0)
	gb.ipad.refresh()
	#curses.mousemask(curses.ALL_MOUSE_EVENTS)
	gb.cscrn = gb.ipad
	gb.cscrn.refresh()
	
	while True:
		# get user command
		ch = gb.cscrn.scrn.getch()
		gb.cscrn.scrn.echochar(ch)
		# Back space == curses.KEY_BACKSPACE
		while ch != 10:		# 10 is ASCII rep of RETURN key
			if ch == 27:
				break
			if ch < 128 and ch > 31:
				gb.inp += chr(ch)
			ch = gb.cscrn.scrn.getch()
			if ch == curses.KEY_BACKSPACE:
				gb.inp = gb.inp[:-1]
				gb.cscrn.scrn.clear()
				gb.cscrn.scrn.addstr(0,0,gb.inp)
				gb.cscrn.refresh()
			elif ch < 128 and ch > 31:
				gb.cscrn.scrn.echochar(ch)
				
		if ch == 27:
			break
			
		apadyx = gb.apad.scrn.getyx()
		gb.apad.scrn.addstr(apadyx[0], 0, gb.inp)
		
		inc = len(gb.inp)/(gb.apad.smaxcol-gb.apad.smincol)+1

		if apadyx[0] - gb.apad.pminrow == maxyx[0] - 1:
			gb.apad.pminrow += inc
		gb.apad.scrn.move(apadyx[0] + inc, 0)
		gb.apad.refresh()
		
		gb.cscrn.scrn.clear()
		gb.cscrn.refresh()
		gb.inp = ""

		"""
		if c == curses.KEY_MOUSE:
			gb.mouse = curses.getmouse()
			curpadbound = (gb.curscrn.sminrow, gb.curscrn.smaxrow)
			# device_id, mouse_x, mouse_y, mouse_z, eventType
			if(gb.mouse[4] == 4) or (gb.mouse[4] == 8):
				if(gb.mouse[2] < curpadbound[0] or gb.mouse[2] > curpadbound[1]):
					movetoothersubwin()
				if(gb.mouse[2] > gb.IWscrn.sminrow and gb.mouse[2] < gb.IWscrn.smaxrow):
					num = readNum(gb.mouse[1]+gb.curscrn.pmincol, (gb.mouse[2] + gb.curscrn.pminrow - gb.curscrn.sminrow))
					if num != -1:						
						highlightIndex((gb.mouse[2] - gb.curscrn.sminrow-1),indexObj,num)
						gb.SWscrn.pminrow = pdfObj.pageindex[num-1]
						gb.SWscrn.scrn.move(pdfObj.pageindex[num-1],0)
						gb.SWscrn.refresh()
						gb.IWscrn.refresh()
					gb.curscrn.scrn.move((gb.mouse[2] + gb.curscrn.pminrow - gb.IWscrn.sminrow), gb.mouse[1]+gb.curscrn.pmincol)
					gb.curscrn.currow = gb.mouse[2] - gb.IWscrn.sminrow
					gb.curscrn.curcol = gb.mouse[1] 
					gb.curscrn.refresh()
					"""
		#if c == curses.KEY_UP:
		#elif c == curses.KEY_DOWN:
		#elif c == curses.KEY_LEFT:
		#elif c == curses.KEY_DOWN:
		
		#if c < 256:
		#while c != curses.KEY_ENTER:
			
		#if string != None:
		
		#elif:
		#c = chr(c)
			
			#elif c == 'v' and gb.curscrn == gb.IWscrn:
			#y,x = gb.curscrn.scrn.getyx()
			#	num = readNum(x,y)
			#	if num != -1:
			#		highlightIndex((y-1),indexObj,num)
			#		gb.SWscrn.pminrow = pdfObj.pageindex[num-1]
			#		gb.SWscrn.scrn.move(pdfObj.pageindex[num-1],0)
			#		gb.SWscrn.refresh()
			#		gb.IWscrn.refresh()
			#	gb.curscrn.scrn.move(y, x)
			#	gb.curscrn.refresh()
			#elif c == 'q': break
	
	restorescreen()
	print gb.apad.scrn.getyx()
	print maxyx
	print ch

def restorescreen():
	curses.nocbreak()
	curses.echo()
	curses.endwin()
	
if __name__ == '__main__':
	main()
