import curses, os, sys, traceback, string, time

"""
global variables for CURSES
"""
class gb:
	# HW3 
	cscrn = None	# Current screen/pad
	pscrn = None	# Parent screen
	bpad = None	# Bids pad
	apad = None	# Annce pad
	ipad = None	# Input pad
	
	inp = ""	# input string
	
			

	mouse = ()	# device_id, mouse_x, mouse_y, mouse_z, eventType	
	
	maxys = None


"""
Screen object to help deal with pads easier - refresh(...)
"""
class screen:
	def __init__(self, scrn):
		self.scrn = scrn

		# pad.refresh(pminrow,pmincol,sminrow,smincol,smaxrow,smaxcol)
		self.pminrow = 0
		self.pmincol = 0
		self.sminrow = 0
		self.smaxrow = 0
		self.smincol = 0
		self.smaxcol = 0
		# pad.refresh(pminrow,pmincol,sminrow,smincol,smaxrow,smaxcol)

		# current row and col for pads. for scrolling purpose
		self.currow = 0
		self.curcol = 0
		self.maxyx = scrn.getmaxyx()
	
	# Refresh helper function to refresh pads
	def refresh(self):
		if self.scrn != None:
			self.scrn.refresh(self.pminrow, self.pmincol, self.sminrow, self.smincol, self.smaxrow, self.smaxcol)
'''
# Move to the other sub window
def movetoothersubwin():
        if gb.curscrn == gb.SWscrn:
                gb.curscrn = gb.IWscrn
        elif gb.curscrn == gb.IWscrn:
		gb.curscrn = gb.SWscrn
        gb.curscrn.refresh()	# REFRESH SETS "ACTIVE" WINDOW

# Move one line up or down and scroll if necessary 
def moveonelineupdown(inc):
	curyx = gb.curscrn.scrn.getyx()
	parentmaxyx = gb.parent.maxyx
	curmaxyx = gb.curscrn.maxyx
	oldPage = gb.pdfObj.whatpage(curyx[0])
	newPage = gb.pdfObj.whatpage(curyx[0]+inc)


	if(inc == 1):		
		if(gb.curscrn.currow < (gb.curscrn.smaxrow - gb.curscrn.sminrow)):
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
			gb.curscrn.currow += inc
		elif(curyx[0] < curmaxyx[0] - 1):
			gb.curscrn.pminrow += inc
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])		
	else:
		if(gb.curscrn.currow > 0):
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
			gb.curscrn.currow += inc
		elif(curyx[0] > 0):
			gb.curscrn.pminrow += inc 
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
	gb.curscrn.refresh()
'''

def init(isServer = 0):
	# window setup
	gb.cscrn = screen(curses.initscr())
	
	if curses.has_colors():
		curses.start_color()
		curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_RED)
		curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_GREEN)


	gb.pscrn = gb.cscrn	# parent screen pointer
	#curses.noecho()
	# curses.cbreak()
	curses.def_prog_mode()
	
	# getmaxyx return a tuple (y, x) of h and w of the window
	gb.maxyx = gb.cscrn.scrn.getmaxyx()
	if(gb.maxyx[0] < 24 and gb.maxyx[1] < 80):
		print "Minimum terminal size: 80 x 24"
		return
		
	half = gb.maxyx[0]/2, gb.maxyx[1]/2	# To split screen halfway

	#intialize pad screens
	gb.apad = screen(curses.newpad(gb.maxyx[0] * 2, half[1]-2))
	gb.bpad = screen(curses.newpad(gb.maxyx[0] * 2, half[1]-2)) 
	gb.ipad = screen(curses.newpad(half[0], half[1]))

	
	#allow keypad for mouse
	gb.apad.scrn.keypad(1)
	gb.bpad.scrn.keypad(1)
	gb.ipad.scrn.keypad(1)

	#define edges for apad
	gb.apad.sminrow = 1
	gb.apad.smaxrow = gb.maxyx[0]-1
	gb.apad.smincol = half[1]
	gb.apad.smaxcol = gb.maxyx[1]-1

	#define edges for bpad
	gb.bpad.sminrow = 1
	gb.bpad.smaxrow = half[0] - 1
	gb.bpad.smincol = 0
	gb.bpad.smaxcol = half[1]-2

	#define edges for ipad
	gb.ipad.sminrow = half[0]+1
	gb.ipad.smaxrow = gb.maxyx[0]-1
	gb.ipad.smincol = 0
	gb.ipad.smaxcol = half[1]-2

	#gb.curscrn = gb.IWscrn

	for i in range(0,gb.maxyx[0]):
		gb.pscrn.scrn.addstr(i,half[1]-1, "|")

	#set labels from parent screen
	gb.pscrn.scrn.addstr(0,0, " === Bids === ", curses.A_REVERSE)
	if isServer:
		gb.pscrn.scrn.addstr(half[0],0, " === Auctioneer's Input === ", curses.A_REVERSE)
	else:
		gb.pscrn.scrn.addstr(half[0],0, " === Bidder's Input === ", curses.A_REVERSE)
	gb.pscrn.scrn.addstr(0,half[1], " === Announcements === ", curses.A_REVERSE)
	gb.pscrn.scrn.refresh()
	
	#set intial cursor
	gb.apad.scrn.move(0,0)
	gb.apad.refresh()
	gb.bpad.scrn.move(0,0)
	gb.bpad.refresh()
	gb.ipad.scrn.move(0,0)
	gb.ipad.refresh()
	#curses.mousemask(curses.ALL_MOUSE_EVENTS)
	gb.cscrn = gb.ipad
	gb.cscrn.refresh()
	

"""
Add string to specified pad
Highlighting flag is optional
0 is white on black
1 is black on white
2 is white on red
3 is white on green
"""
def addToScrn(cscrn, string, highlight = 0):
	
	padyx = cscrn.scrn.getyx()
	paddim = cscrn.scrn.getmaxyx()
	# TO CHECK IF STRING LENGTH > HALF OF APAD'S DIMENSION
	padarea = (paddim[0] * paddim[1]) / 2
	if len(string) > padarea:
		string = string [:padarea]

	inc = len(string)/(cscrn.smaxcol-cscrn.smincol)+1
	# DELETE THE TOP IF FULL
	if (padyx[0] + inc) >= (cscrn.maxyx[0] -1):
		for i in range(inc):
			cscrn.scrn.move(0,0)
			cscrn.refresh()
			cscrn.scrn.deleteln()
			cscrn.refresh()
		padyx = [padyx[0]-inc, padyx[1]]
	# HIGHLIGHTING
	if highlight == 1 :
		cscrn.scrn.addstr(padyx[0], 0, string, curses.A_REVERSE)
	elif highlight == 0 :
		cscrn.scrn.addstr(padyx[0], 0, string)
	else:
		cscrn.scrn.addstr(padyx[0], 0, string, curses.color_pair(highlight))
		
	if padyx[0] - cscrn.pminrow + inc > cscrn.smaxrow:
		cscrn.pminrow = padyx[0] + inc - cscrn.smaxrow
	cscrn.scrn.move(padyx[0]+inc, 0)
	cscrn.refresh()
	gb.ipad.refresh()
"""
Add string to announcement pad
"""
def addToAnnouncements(string, highlight = 0):
	addToScrn(gb.apad, string, highlight)

"""
Add string to bid pad
"""
def addToBids(string, isYours = 0):
	addToScrn(gb.bpad, string, isYours)

	
def getInput():
    #try:
        #time.sleep(2)
        ch = gb.cscrn.scrn.getch()
        if ch == 27:
            return -1
        if ch != 10:		# 10 is ASCII rep of RETURN key
            if  ch > 31 and ch < 128:
                gb.inp += chr(ch)
            if ch == curses.KEY_BACKSPACE:
                gb.inp = gb.inp[:-1]
                gb.cscrn.scrn.clear()
                gb.cscrn.scrn.addstr(0,0,gb.inp)
                gb.cscrn.refresh()
            elif ch > 31 and ch < 128:
                gb.cscrn.scrn.echochar(ch)
        elif ch == 10:
            gb.cscrn.scrn.clear()
            gb.cscrn.refresh()
            temp = gb.inp
            gb.inp = ""
            if temp == "\n" or temp == "":
                return 0
            return temp
        return 0
    #except KeyboardInterrupt:
        #restorescreen()
        #return -1

def restorescreen():
	curses.nocbreak()
	curses.echo()
	curses.endwin()

