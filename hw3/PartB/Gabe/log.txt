[[100, 2.0, 2, 2.0, 0.1, 0.1], [100, 2.0, 2, 2.0, 0.1, 10.0], [2, 2.0, 2, 2.0, 0.1, 10.0], [4, 2.0, 2, 2.0, 0.1, 10.0], [20, 4.0, 5, 7.0, 10.0, 10.0], [2, 5.0, 2, 1.0, 0.01, 0.01]]
it doesn't look like you are using the right arguments.
using the default arguments for now: (100, 2.0, 2, 2.0, 0.1, 0.1)
[100, 2.0, 2, 2.0, 0.1, 0.1]
['rejected ratio', 0.002036659877800407, 'busyNodes', 0.18200408997955012, 'userArrivals', 2.0431198346169417, 'numberNodesRequested', 2.0, 'numberJobsSpawned', 1.9631901840490797, 'mapProcessingTimes', 0.10158044144237628, 'redProcessingTimes', 0.10025947839546266, 'wait times', 'MapPhase', 0.09534346760487121, 'RedPhase', 0.019189446914961873]
[100, 2.0, 2, 2.0, 0.1, 10.0]
['rejected ratio', 0.0019305019305019305, 'busyNodes', 10.309055118110237, 'userArrivals', 1.934932479682267, 'numberNodesRequested', 2.0, 'numberJobsSpawned', 2.0174418604651163, 'mapProcessingTimes', 0.1011343704783653, 'redProcessingTimes', 9.883514838917979, 'wait times', 'MapPhase', 0.10067884679227332, 'RedPhase', 9.474394028208822]
[2, 2.0, 2, 2.0, 0.1, 10.0]
['rejected ratio', 0.9138576779026217, 'busyNodes', 0.0, 'userArrivals', 1.8804356151740567, 'numberNodesRequested', 2.0, 'numberJobsSpawned', 2.088888888888889, 'mapProcessingTimes', 0.10153896217362648, 'redProcessingTimes', 9.701890843125078, 'wait times', 'MapPhase', 0.10547575280421453, 'RedPhase', 10.453440917708136]
[4, 2.0, 2, 2.0, 0.1, 10.0]
['rejected ratio', 0.7239263803680982, 'busyNodes', 1.7938931297709924, 'userArrivals', 2.067776555223477, 'numberNodesRequested', 2.0, 'numberJobsSpawned', 2.044776119402985, 'mapProcessingTimes', 0.09954890410552306, 'redProcessingTimes', 9.909308316257992, 'wait times', 'MapPhase', 0.10127972611515046, 'RedPhase', 9.782178696206186]
[20, 4.0, 5, 7.0, 10.0, 10.0]
['rejected ratio', 0.48497854077253216, 'busyNodes', 15.74074074074074, 'userArrivals', 4.310602611225351, 'numberNodesRequested', 3.5757575757575757, 'numberJobsSpawned', 7.168067226890757, 'mapProcessingTimes', 9.945449289275572, 'redProcessingTimes', 9.991939739097228, 'wait times', 'MapPhase', 27.152592301482287, 'RedPhase', 46.786107896654784]
[2, 5.0, 2, 1.0, 0.01, 0.01]
['rejected ratio', 0.009950248756218905, 'busyNodes', 0.0, 'userArrivals', 5.0054575465422335, 'numberNodesRequested', 2.0, 'numberJobsSpawned', 1.0, 'mapProcessingTimes', 0.010018727734528889, 'redProcessingTimes', 0.00990387784842278, 'wait times', 'MapPhase', 0.0, 'RedPhase', 0.0]
