#/usr/bin/env python

import socket, sys, select, server_curses, sendEmail

server_curses.init(1)

"""python aucsrvr.py bidder_file server_port"""

class Bid:

	def __init__(self, iden, item, amount):
		self.ID = iden
		self.Item = item
		self.Amount = amount
		
	

class Announcements:
	bidOpen = "The bidding is open."
	bidClosed = "The bidding is closed."
	bidWinnder = "The bid winner is: "

class Item:
	def __init__(self, num, win, amount):
		self.Number = num
		self.CurrentWinner = win
		self.CurrentAmount = amount
		self.isOpen = False

	def Open(self):
		self.isOpen = True
	def Close(self):
		self.isOpen = False

class Server:
	size = 1024
	input = [] #live connection list
	users = [] #Users expected
	emails = {} #Dictionary of emails, Tuples are keys
	numusers = 0 #Number of users
	numcons = 0 #Number of connections
	announcements = []
	bids = []
	items = []
	numitems = 0
	

	def __init__(self, bidderfile, server_port):
		self.parseUsers(bidderfile)
		for email in self.users:
			server_curses.addToAnnouncements(email)
		#print "Number of Users Expected: "+str(self.numusers)
		conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		host = ''
		port = int(server_port)
		conn.bind((host,port))
		#print "Listening"
		conn.listen(self.numusers)
		self.input = [conn, sys.stdin]
		running = 1
		while running:
			try:
				inputready, outputready,exceptready = select.select(self.input, [], [])
			except KeyboardInterrupt:
				server_curses.restorescreen()
				conn.close() # close socket
				sys.exit()
			for option in inputready:
				if option == conn and self.numcons < self.numusers:
					client, addr = conn.accept()
					server_curses.addToAnnouncements("This Client is Connected:")
					server_curses.addToAnnouncements(str(client.getpeername()))
					client.sendto("0 Please Enter Your Email Address.", addr)
					#append to list of live connections
					self.input.append(client)
					#increment number of current connections
					self.numcons += 1
					server_curses.addToAnnouncements("Number of users: "+str(self.numcons))
					#print "Number of connections is: ", self.numcons
				elif option == sys.stdin:
					command = server_curses.getInput()
					if (str(command).lower()).startswith("new item"):
						iteminfo = command.split()
						if len(iteminfo) != 4:
							#print Bad Item to screen
							continue
						else:	
							server_curses.addToAnnouncements(str(command))
							newitem = Item(int(iteminfo[2]), None, float(iteminfo[3]))
							self.items.append(newitem)
							self.numitems += 1
					elif str(command).lower() == "the auction is open":
						self.items[self.numitems-1].Open()
						for cl in self.input:
							if cl != sys.stdin and cl != conn:
								cl.sendto("0 "+command, cl.getpeername())
					elif str(command).lower() == "the bidding is open":
						if self.numitems > 0:
							self.items[self.numitems-1].Open()
							for cl in self.input:
								if cl != sys.stdin and cl != conn:
									cl.sendto("0 "+command+'\n'+ "Item Number: "+str(self.items[self.numitems-1].Number)+" Starting Price: $%.2f"%(self.items[self.numitems-1].CurrentAmount), cl.getpeername())
						server_curses.addToAnnouncements("Announcement: " + str(command))
					elif str(command).lower() == "the bidding is closed":
						self.items[self.numitems-1].Close()
						for cl in self.input:
							if cl != sys.stdin and cl != conn:
								cl.sendto("0 "+command, cl.getpeername())
					# ESC key or "quit" command
					elif str(command).lower() == "the auction is closed" or str(command).lower() == "quit" or command == -1:
						for cl in self.input:
							if cl != sys.stdin and cl != conn:
								cl.sendto("0 "+command, cl.getpeername())
						sendEmail.sendEmailsToWinners(self.items, self.emails)
						server_curses.restorescreen()
						self.input.remove(option)
						option.close() # close socket
						sys.exit()
					elif command != 0:
						for cl in self.input:
							if cl != sys.stdin and cl != conn:
								cl.sendto("0 "+command, cl.getpeername())
						server_curses.addToAnnouncements("Announcement: "+str(command))
				elif option in self.input:
					data = option.recv(self.size)
					if data:
						if data in self.users:
							if option in self.emails:
								#user has logged in already
								option.sendto("0 You are already logged in as " + self.emails[option] , option.getpeername())
							else:
								self.emails[option] = data
								option.sendto("0 Successfully Logged In.", option.getpeername())
						elif option not in self.emails:
							option.sendto("0 You Have Not Logged In Retry.", option.getpeername())
						else:
							#Parse Bid
							newbid = self.parseBid(data, option)
							#Determine if Bid is legit
							response = self.determineBid(newbid)
							#Send response
							option.sendto(response, option.getpeername())
							if self.numitems > 0:
								for cl in self.input:
									if cl != sys.stdin and cl != conn and cl != option:
										cl.sendto("1 Highest Bid on Item "+str(self.items[self.numitems-1].Number)+" is: $%.2f"%(self.items[self.numitems-1].CurrentAmount), cl.getpeername())
								server_curses.addToBids("Highest Bid on Item  "+str(self.items[self.numitems-1].Number)+" is: $%.2f"%(self.items[self.numitems-1].CurrentAmount))
					else:
						server_curses.addToAnnouncements("Client disconnected.")
						self.numcons -= 1
						self.input.remove(option)
						server_curses.addToAnnouncements(str(option.getpeername()))
						server_curses.addToAnnouncements("Number of Users: "+str(self.numcons))
						option.close()
				else:
					continue

	def parseUsers(self,filename):
		file = open(filename)
		lines = file.readlines()
		lines = [line.strip() for line in lines]
		for user in lines:
			self.users.append(user)
			self.numusers += 1

	def parseBid(self, bid, user):
		bid = bid.split()
		if(len(bid) != 2):
			return None
		ID = user
		Item = int(bid[0])
		Amount = float(bid[1])
		newbid = Bid(ID,Item,Amount)
		return newbid

	def determineBid(self, bid):
		#print "in determine bid"
		if (bid == None):
			return "2 Bad Bid."
		elif (len(self.items) <= 0):
			return "2 No current bids"
		elif not self.items[self.numitems-1].isOpen:
			return "2 Bidding has ended on Item: "+str(bid.Item)
		elif bid.Item != self.items[self.numitems-1].Number:
			return "2 Item: "+str(bid.Item)+" is not open for sale."
		elif self.items[self.numitems-1].CurrentAmount < float(bid.Amount):
			self.items[self.numitems-1].CurrentWinner = bid.ID
			self.items[self.numitems-1].CurrentAmount = float(bid.Amount)
			self.bids.append(bid)
			return "3 You bid placed a higher bid on Item: "+str(bid.Item)+" for $%.2f"%float(bid.Amount)
		else:
			return "2 Your bid was not high enough on Item: "+str(bid.Item)+" It is $%.2f"%(self.items[self.numitems-1].CurrentAmount)
			
if __name__ == '__main__':
    serverObj = Server(sys.argv[1], sys.argv[2])
