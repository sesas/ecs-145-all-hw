#/usr/bin/env python

import socket, sys, select, server_curses, sendEmail, traceback
import random as r

server_curses.init(1)

"""python aucsrvr.py bidder_file server_port"""

class G:
	error = {}

class Bid:

	def __init__(self, iden, item, amount):
		self.ID = iden
		self.Item = item
		self.Amount = ((amount * 100)//1)/100	
	

#class Announcements:
	#bidOpen = "The bidding is open."
	#bidClosed = "The bidding is closed."
	#bidWinnder = "The bid winner is: "

class Item:
	def __init__(self, num, win, amount):
		self.Number = num
		self.CurrentWinner = win
		self.CurrentWinnerEmail = None
		self.CurrentAmount = amount
		self.isOpen = False
	
	def __str__(self):
		status = 'CLOSED'
		if self.isOpen:
			status = 'OPEN'
		s = 'The current item is item # %i, the top bid is by %s, with $%.2f. the bid is currently %s.' % (self.Number, self.CurrentWinnerEmail, self.CurrentAmount, status)
		return s

	def Open(self):
		self.isOpen = True
	def Close(self):
		self.isOpen = False

class Server:
	input = [] #live connection list
	users = [] #Users expected
	emails = {} #Dictionary of emails, Tuples are keys
	numusers = 0 #Number of users
	numcons = 0 #Number of connections
	announcements = []
	bids = []
	items = []
	numitems = 0
		
	list_of_file_writes = {}	# key is a flo_r obj, value is flo_w obj
	
	STATE_SERVER_UP = 1
	STATE_AUCTION_BEGIN = 2
	STATE_NEW_ITEM = 3
	STATE_BIDDING_BEGIN = 4
	STATE_BIDDING_END = 5
	STATE_AUCTION_CLOSED = 6
	STATE_SEND_EMAILS = 7
	STATE_SERVER_DOWN = 8
	
#	currentState = STATE_SERVER_UP
	workflow = {STATE_AUCTION_BEGIN: [3, 6],
				STATE_AUCTION_CLOSED: [7, 6],
				STATE_BIDDING_BEGIN: [5],
				STATE_BIDDING_END: [6, 3],
				STATE_NEW_ITEM: [4, 6],
				STATE_SEND_EMAILS: [8],
				STATE_SERVER_UP: [3, 2, 6]}
	
	possibleCommands = ['new item item# starting_price', 
					'the auction is open',  
					'the bidding is open', 
					'the bidding is closed',
					'the auction is closed', 
					'quit',
					'or you can press CTRL+C to quit as well.',
					]

	def changeStateTo(self, newState):
		if newState in self.workflow[self.currentState]:
			self.currentState = newState
			return True
		else:
			return False

	def __init__(self, bidderfile, server_port):
		self.currentState = self.STATE_SERVER_UP
		self.parseUsers(bidderfile)
		
		# Print the list of all email addressed inside the bidder_file
		server_curses.addToAnnouncements("The possible bidders are:")
		for email in self.users:
			server_curses.addToAnnouncements(email)
		
		server_curses.addToAnnouncements("The possible auctioneer's commands are:")
		for command in self.possibleCommands:
			server_curses.addToAnnouncements('- ' + command)
		
		
		# create an internet and tcp/ip socket
		conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		host = ''
		port = int(server_port)
		conn.bind((host,port))
		
		# listen for a total of numsers users (default 5)
		conn.listen(self.numusers)
		
		# the list of events that select() listens for
		self.input = [conn, sys.stdin]
		running = 1
		while running:
			try:
				try:
					inputready, outputready,exceptready = select.select(self.input, [], [])
				except KeyboardInterrupt:
					# we typed Ctrl+C
					self.closeAll()
					continue

				# randomize the input queue for "fairness"
				r.shuffle(inputready)
				
				for option in inputready:
				
					# 1. The event is a NEW client trying to connect to us
					if option == conn and self.numcons < self.numusers:
						# accept this connection, creating a client/addr tuple
						client, addr = conn.accept()
						
						# we must create a unique file handle for our makefile
						flo_w = client.makefile('w',0)	# file descriptor to send back to the client
						flo_r = client.makefile('r',0)	# file descriptor to READ from the client

						# append to list of live connections for select()
						self.input.append(flo_r)
						
						# we create a dict to associate read file handles to write file handles
						self.list_of_file_writes[flo_r] = flo_w
						
						# increment number of current connections
						self.numcons += 1
						
						# we add to the auctioneer's announcement everytime a new client connects
						server_curses.addToAnnouncements("This client is connected, not yet logged in: " + str(client.getpeername()))
						#server_curses.addToAnnouncements()
						
						# send to the client
						self.unicast("Please enter your email address.", flo_r)
						
						server_curses.addToAnnouncements("Number of users: "+str(self.numcons))
						
					# 2. The event is from standard in (most likely keyboard)
					elif option == sys.stdin:
						# call our curses method, getch(), and append to input until "Enter" is pressed
						command = str(server_curses.getInput()).lower().strip()
						
						
						if command.startswith("new item") or command.startswith('f1'):
							iteminfo = command.split()
							# if command.startswith('f1'):
							#	iteminfo = 'new item 2 32'.split()
								
							try:
								if len(iteminfo) != 4:
									raise Exception
								
								if not self.changeStateTo(self.STATE_NEW_ITEM):
									server_curses.addToAnnouncements("Cannot start a new item at this time. Did you close the last bidding?", 2)
									continue
									
								already_made = 0
								# check for duplicate new item entries
								for item_objs in self.items:
									if item_objs.Number == int(iteminfo[2]):
										already_made = 1
										break
										
										
								if already_made:
									server_curses.addToAnnouncements("This item has already been added", 2)
								else:
									# make sure the 1st number is int() and 2nd is float()
									newitem = Item(int(iteminfo[2]), None, (float(iteminfo[3])*100//1)/100)
									self.items.append(newitem)
									self.numitems += 1
								
								
									server_curses.addToAnnouncements("Announcement: "+command)
							except:
								server_curses.addToAnnouncements("Bad 'new item' format.", 2)
								continue


						elif command in ["the bidding is open", 'f2']:
							if not self.changeStateTo(self.STATE_BIDDING_BEGIN): 
								server_curses.addToAnnouncements("Cannot start bidding at this time. Did you create a new item already?", 2)
								continue
							self.items[-1].Open() 
							self.broadcast(command+", Item number: "+str(self.items[-1].Number)+" Starting price: "+str(self.items[-1].CurrentAmount))
							server_curses.addToAnnouncements("Announcement: "+command)


						elif command in ["the bidding is closed", 'f3']:
							try:
								if not self.changeStateTo(self.STATE_BIDDING_END): 
									server_curses.addToAnnouncements("Cannot end bidding at this time. Did you open the bidding in the first place?", 2)
									continue
								server_curses.addToAnnouncements("Announcement: "+command)
								if self.items[-1].CurrentWinner:
									self.broadcast(command + ", " + "The winner is: "+ self.items[-1].CurrentWinnerEmail)
								else:
									self.broadcast(command +', ' + 'nobody won the item.')
									# delete the item from item list because no one won it
									self.items[-1].Close()
							except: 
								gl.error['bid closing'] = [command, 
															self.currentState, 
															]
								raise
															

						# ESC key or "quit" command
						elif command in ["the auction is closed", "the auction is over", "quit", '-1', "f4"]:
							self.closeAll()
							continue
							
						# This is a REGULAR announcement
						elif command != '0':
							self.broadcast(command)
							server_curses.addToAnnouncements("Announcement: "+command)
					
					# 3. File READ object is waiting to be read. Aka client is trying to send us data
					elif option in self.input:
						# read a single command from the file handler
						data = option.readline()
						data = data.strip()
						if data:
							try:
								if data in self.users:
									if option in self.emails:
									#if data in self.emails.values():
										# user has logged in already
										self.unicast("You are already logged in as " + self.emails[option], option)
									elif data in self.emails.values():
										self.unicast("That user is already logged in from elsewhere.", option)
									else:
										self.emails[option] = data
										self.unicast("Successfully logged in.", option)
										server_curses.addToAnnouncements("The user " + data + " has logged in.", 1)
								elif option not in self.emails:
									# we could not find the entered in email to match our bidder_file
									self.unicast("You have not logged in. Retry.", option)
									
								elif data.startswith('mess:'):
									# the user want's to send a message to the auctioneer
									email = self.emails[option]
									server_curses.addToAnnouncements(email + ' ' + data)
										
								elif data.lower().strip() in 'status':
									#mess = []
									#mess.append(str(self.items[-1]))
									if self.items:
										self.unicast(str(self.items[-1]), option)
									else:
										self.unicast("There are no items to bid on yet", option)
									
								else:
									# Client is sending us a BID
									if self.currentState == self.STATE_BIDDING_BEGIN:
										# Parse Bid
										newbid = self.parseBid(data, option)
										# Determine if Bid is legit
										response = self.determineBid(newbid)
										# Send response back to the client
										self.unicast(response[2:], option, int(response[0:1]))
										
										if self.numitems > 0 and response[0] == '3':
											# This was a VALID new bid. Broadcast a message to each client
											self.broadcast("Highest bid on item "+str(self.items[self.numitems-1].Number)+" is: $%.2f"%(self.items[self.numitems-1].CurrentAmount), [option], 1)
											server_curses.addToBids("Highest bid on item  "+str(self.items[self.numitems-1].Number)+" is: $%.2f"%(self.items[self.numitems-1].CurrentAmount))
									else:
										self.unicast("The bidding on this item has closed", option, 2)
							except Exception as inst:
								with open('error.txt', 'w') as f:
									gl.error['loggin in'] = [data, option, self.emails, 
															data in self.emails.values(), 
															option in self.emails, 
															data in self.users, 
															inst, inst.args,
															str(inst),
															traceback.print_exc(file=f),
															]
								raise
								
						else:
							if option in self.emails:
								email = self.emails.pop(option)
								server_curses.addToAnnouncements( email + " client disconnected.")
							else: 
								server_curses.addToAnnouncements( str(client.getpeername()) + " client disconnected.")
							self.numcons -= 1
							self.input.remove(option)
							self.list_of_file_writes.pop(option)
							server_curses.addToAnnouncements("Number of connections: "+str(self.numcons))
							option.close()
			except Exception as inst:
				with open('error.txt', 'w') as f:
					gl.error['general error'] = [str(inst), traceback.print_exc(file=f)]
					self.currentState = self.STATE_AUCTION_CLOSED
					self.closeAll()
	""" 
	Close yourself and all the clients 
	"""					
	def closeAll(self):
		if not self.changeStateTo(self.STATE_AUCTION_CLOSED): 
			server_curses.addToAnnouncements("Cannot end the auction at this time. Try ending the bidding first.", 2)
			return
		self.broadcast("The auction has closed. Have a good day.")
		self.broadcast("Closing client", [], 4)
#		for flo_w in self.list_of_file_writes.values():
#			flo_w.write("0 The auction has closed. Have a good day. \n")
#			flo_w.write("4 Closing client \n")	
		if not self.changeStateTo(self.STATE_SEND_EMAILS): 
			server_curses.addToAnnouncements("Cannot send emails at this time.", 2)
			return
		if self.items:
			sendEmail.sendEmailsToWinners(self.items, self.emails)
		server_curses.restorescreen()
		#I think these are not needed 
		#self.input.remove(option)
		
		#option.close() # close socket
		sys.exit()
				
	def unicast(self, messageStr, targetClient, priority = 0):
		if messageStr[-1] != '\n':
			messageStr += '\n'
		try:
			self.list_of_file_writes[targetClient].write(str(priority) + " " + messageStr)
		except:
			gl.error['unicast'] = messageStr, targetClient, priority
			
	def broadcast(self, messageStr, exceptionList = [], priority = 0):
		"""
		It will send a message to all clients except those in the exception list. 
		No new-line char is needed at the end of the message.
		The exception list might only contain [option]
		"""
		if messageStr[-1] != '\n':
			messageStr += '\n'
		for flo_r in self.list_of_file_writes.keys():
			if flo_r not in exceptionList:
				try:
					self.list_of_file_writes[flo_r].write(str(priority) + ' ' + messageStr)
				except:
					pass

	def parseUsers(self,filename):
		file = open(filename)
		lines = file.readlines()
		lines = [line.strip() for line in lines]
		for user in lines:
			self.users.append(user)
			self.numusers += 1

	def parseBid(self, bid, user):
		try:
			bid = bid.split()
			if(len(bid) != 2):
				return None
			ID = user
			Item = int(bid[0])
			Amount = float(bid[1])
			newbid = Bid(ID,Item,Amount)
			return newbid
		except:
			return None

	def determineBid(self, bid):
		#print "in determine bid"
		try:
			if bid == None or not self.items or bid.Item != self.items[-1].Number:
				return "2 Bad Bid."
			elif (len(self.items) <= 0):
				return "2 No current bids"
			elif not self.items[-1].isOpen:
				return "2 Bidding has ended on Item: "+str(bid.Item)
			elif bid.Item != self.items[self.numitems-1].Number:
				return "2 Item: "+str(bid.Item)+" is not open for bidding."
			#elif self.emails[self.items[-1].CurrentWinner] == self.emails[bid.ID]:
			#elif self.items[-1].CurrentWinner == bid.ID:
			elif self.items[-1].CurrentWinnerEmail == self.emails[bid.ID]:
				return "2 You are already the top bidder."
			elif self.items[-1].CurrentAmount < float(bid.Amount):
				self.items[-1].CurrentWinner = bid.ID
				self.items[-1].CurrentWinnerEmail = self.emails[bid.ID]
				self.items[-1].CurrentAmount = float(bid.Amount)
				self.bids.append(bid)
				return "3 You bid placed a higher bid on Item: "+str(bid.Item)+" for $%.2f"%float(bid.Amount)
			else:
				return "2 Your bid was not high enough on Item: "+str(bid.Item)+" It is $%.2f"%(self.items[self.numitems-1].CurrentAmount)
		except Exception as inst:
			#server_curses.addToAnnouncements(str(e))
			return "2 Bad Bid. " #+ type(inst)
			
			
if __name__ == '__main__':
	gl = G()
	try:
		serverObj = Server(sys.argv[1], sys.argv[2])
	except:
		if gl.error:
			print gl.error
