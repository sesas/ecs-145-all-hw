# strsvr.py: simple illustration of nonblocking sockets
# multiple clients connect to server; each client repeatedly sends a
# letter k, which the server adds to a global string v and echos back
# to the client; k = '' means the client is dropping out; when all
# clients are gone, server prints final value of v

# this is the server; usage is
# 	python strsvr.py server_port_number

import socket, sys
# set up listening socket
lstn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = int(sys.argv[1])
# bind lstn socket to this port
lstn.bind(('', port))
lstn.listen(5)

# initialize concatenate string, v
v = ''

# initialize client socket list
cs = []

# in this example, a fixed number of clients, nc
nc = 2

# accept connections from the clients
for i in range(nc):
	(clnt,ap) = lstn.accept()
	# set this new socket to nonblocking mode
	clnt.setblocking(0)
	cs.append(clnt)

# now loop, always accepting input from whoever is ready, if any, until
# no clients are left
while (len(cs) > 0):
	# get next client, with effect of a circular queue
	clnt = cs.pop(0)
	cs.append(clnt)
	# something ready to read from clnt? clnt closed connection?
	try:
		k = clnt.recv(1)	# try to receive one byte; if none is ready
											# yet, that is the "exception"
		if k == '':
			clnt.close()
			cs.remove(clnt)
		v += k # update the concatenated list
		clnt.send(v)
	except: pass

lstn.close()
print 'the final value of v is', v

