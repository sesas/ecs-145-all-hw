# JACK WAS HERE
# strclnt.py: simple illustration of nonblocking sockets

# two clients connect to server; each client repeatedly sends a letter k,
# which the server appends to a global string v and reports it to the
# client; k = '' means the client is dropping out; when all clients are
# gone, server prints the final string v

# this is the client; usage is
# python clnt.py server_address server_port_number


import socket, sys

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = sys.argv[1] # server address
port = int(sys.argv[2]) # server port
s.connect((host, port))

while(1):
	# get letter
	k = raw_input('enter a letter:')
	s.send(k) # send k to server
	# if stop signal, then leave loop
	if k == '': break
	v = s.recv(1024)	# receive v from server (up to 1024 bytes, assumed
										# to come in one chunk)
	
	print v

s.close() # close socket
