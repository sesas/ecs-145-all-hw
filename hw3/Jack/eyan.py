import curses, os, sys, traceback, string
import select

"""
global variables for CURSES
"""
class gb:
	# HW3 
	cscrn = None	# Current screen/pad
	pscrn = None	# Parent screen
	bpad = None	# Bids pad
	apad = None	# Annce pad
	ipad = None	# Input pad
	
	inp = ""	# input string
	
	padsMax = {'bpad':[], 'apad':[], 'ipad':[]}	# Dictionary to keep track of maxyx
							# for pads		
			
	prevHighlight = []  # stores record of previously highlighted

	mouse = ()	# device_id, mouse_x, mouse_y, mouse_z, eventType	
	
	SWmaxyx = []	 #max dimension for SW Window
	IWmaxyx = []	 #max dimension for IW Window


"""
Screen object to help deal with pads easier - refresh(...)
"""
class screen:
	def __init__(self, scrn):
		self.scrn = scrn

		# pad.refresh(pminrow,pmincol,sminrow,smincol,smaxrow,smaxcol)
		self.pminrow = 0
		self.pmincol = 0
		self.sminrow = 0
		self.smaxrow = 0
		self.smincol = 0
		self.smaxcol = 0
		# pad.refresh(pminrow,pmincol,sminrow,smincol,smaxrow,smaxcol)

		# current row and col for pads. for scrolling purpose
		self.currow = 0
		self.curcol = 0
		self.maxyx = scrn.getmaxyx()
	
	# Refresh helper function to refresh pads
	def refresh(self):
		if self.scrn != None:
			self.scrn.refresh(self.pminrow, self.pmincol, self.sminrow, self.smincol, self.smaxrow, self.smaxcol)
'''
# Move to the other sub window
def movetoothersubwin():
        if gb.curscrn == gb.SWscrn:
                gb.curscrn = gb.IWscrn
        elif gb.curscrn == gb.IWscrn:
		gb.curscrn = gb.SWscrn
        gb.curscrn.refresh()	# REFRESH SETS "ACTIVE" WINDOW

# Move one line up or down and scroll if necessary 
def moveonelineupdown(inc):
	curyx = gb.curscrn.scrn.getyx()
	parentmaxyx = gb.parent.maxyx
	curmaxyx = gb.curscrn.maxyx
	oldPage = gb.pdfObj.whatpage(curyx[0])
	newPage = gb.pdfObj.whatpage(curyx[0]+inc)


	if(inc == 1):		
		if(gb.curscrn.currow < (gb.curscrn.smaxrow - gb.curscrn.sminrow)):
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
			gb.curscrn.currow += inc
		elif(curyx[0] < curmaxyx[0] - 1):
			gb.curscrn.pminrow += inc
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])		
	else:
		if(gb.curscrn.currow > 0):
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
			gb.curscrn.currow += inc
		elif(curyx[0] > 0):
			gb.curscrn.pminrow += inc
			gb.curscrn.scrn.move(curyx[0] + inc, curyx[1])
	gb.curscrn.refresh()
'''

def initializeScreen():
	# window setup
	gb.cscrn = screen(curses.initscr())
	gb.pscrn = gb.cscrn	# parent screen pointer
	#curses.noecho()
	#curses.cbreak()
	curses.def_prog_mode()  # http://www.mkssoftware.com/docs/man3/curs_kernel.3.asp
#		the link above says that the def_prog_mode() is called automatically by initscr()
		
def initializePads():
	# getmaxyx return a tuple (y, x) of h and w of the window
	maxyx = gb.cscrn.scrn.getmaxyx()
	if(maxyx[0] < 24 and maxyx[1] < 80):
		print "Minimum terminal size: 80 x 24"
		return
		
	half = maxyx[0]/2, maxyx[1]/2	# To split screen halfway

	#intialize pad screens
	gb.apad = screen(curses.newpad(maxyx[0] * 2, half[1]))
	gb.bpad = screen(curses.newpad(maxyx[0] * 2, half[1]))
	gb.ipad = screen(curses.newpad(half[0], half[1]))

	
	#allow keypad for mouse
	gb.apad.scrn.keypad(1)
	gb.bpad.scrn.keypad(1)
	gb.ipad.scrn.keypad(1)

	#define edges for apad
	gb.apad.sminrow = 1
	gb.apad.smaxrow = maxyx[0]-1
	gb.apad.smincol = half[1]
	gb.apad.smaxcol = maxyx[1]-1

	#define edges for bpad
	gb.bpad.sminrow = 1
	gb.bpad.smaxrow = half[0] - 1
	gb.bpad.smincol = 0
	gb.bpad.smaxcol = half[1]

	#define edges for ipad
	gb.ipad.sminrow = half[0]+1
	gb.ipad.smaxrow = maxyx[0]-1
	gb.ipad.smincol = 0
	gb.ipad.smaxcol = half[1]-1

def initializeHeaders():
	#set labels from parent screen
	gb.pscrn.scrn.addstr(0,0, " === Bids === ", curses.A_REVERSE)
	gb.pscrn.scrn.addstr(half[0],0, " === Auctioneer's Input === ", curses.A_REVERSE)
	gb.pscrn.scrn.addstr(0,half[1], " === Announcements === ", curses.A_REVERSE)
	gb.pscrn.scrn.refresh()
		
def setCursorsToStartPosition():
	#set intial cursor
	gb.apad.scrn.move(0,0)
	gb.apad.refresh()
	gb.bpad.scrn.move(0,0)
	gb.bpad.refresh()
	gb.ipad.scrn.move(0,0)
	gb.ipad.refresh()

# Main window object
"""
Main function for curses
"""
def main():	
	try: 
		initializeScreen()
		initializePads()
		#gb.curscrn = gb.IWscrn
		initializeHeaders()
		setCursorsToStartPosition()
	
		#curses.mousemask(curses.ALL_MOUSE_EVENTS)
		gb.cscrn = gb.ipad
		gb.cscrn.refresh()
		while True:	
	
			rlist = [sys.stdin]
			# gb.cscrn.scrn.echochar("I")
			inputready, outputready, exceptready = select.select(rlist,[],[])
			ch = gb.cscrn.scrn.getch()
			gb.cscrn.scrn.echochar(ch)
			break
			gb.cscrn.scrn.echochar("e")
			for s in inputready:
				gb.cscrn.scrn.echochar("r")
				fh = open("test.log", "a")
				fh.write(ch)
				fh.close()
	
				gb.cscrn.scrn.addstr(0,0,str(string))
				gb.cscrn.refresh()
		
#		restorescreen()
		print gb.apad.scrn.getyx()
		print maxyx
		print ch
	finally:
		restorescreen()
		

def restorescreen():
	curses.nocbreak()
	curses.echo()
	curses.endwin()
	
if __name__ == '__main__':
	main()
