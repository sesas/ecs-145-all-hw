David Nguyen - parts of core design of sim, testing
Ken Nickles - initial planning/structuring of code, documentation
Jack Soe - core design of sim, debugging and cross-checking with specs
Edmund Yan - initial planning/structuring of code, testing
Gabriel Reyla - core design and implementation of the sim, debugging 
