
# http://heather.cs.ucdavis.edu/~matloff/145/Hwk/Hwk4.html
# .libPaths("/home/d4chen/ECS145/HW3/ecs-145-all-hw/hw4/")
# source("hw4b.R")

.libPaths("./")
library("pixmap")

mydist <- function(mapfile) {
	img <<- read.pnm(mapfile)
# 	dev.new(width=15, height=12)
	dev.new(width=20, height=15)
# 	win.graph(width = 4, height = 4)
# 	windows(width=8, height=8)
	plot(img, xlab="enter command")
	
# 	vectPoints <- locator(2)
	vectPoints = list()
	vectPoints$y = c(0)
	vectPoints$x = c(0)
	vectPoints$color = c("black", "black")
	allLines <<- list(vectPoints)
	
# 	lines(vectPoints, type = "b")
	scale = 1
	totalDistances = c(0)
# 	print( paste( c("distance =", dist(vectPoints) * scale ), collapse=" "))
	while (1) {
		
# 		lines(vectPoints[len(vectPoints)-2 : len(vectPoints)], type="b")
# 		print( vectPoints )
# 		print(length(vectPoints))
# 		print( length(vectPoints[[1]]) )
# 		print( vectPoints[[1]] )
# 		print( vectPoints[, length(vectPoints[[1]])-1:length(vectPoints[[1]]) ] )
# 		lapply( vectPoints, function(vect) vect[ -0: -(length(vect)-2) ] )
		newPt <<- locator(1)
		if (newPt$y >= 0) {
			numLines = length(allLines)
			allLines[[numLines]]$y <<- c(allLines[[numLines]]$y, newPt$y)
			allLines[[numLines]]$x <<- c(allLines[[numLines]]$x, newPt$x)
# 			allLines[[numLines]]$color <<- c(allLines[[numLines]]$color , allLines[[numLines]]$color[1])
# 			vectPoints$y = c(vectPoints$y, newPt$y)
			vectPoints = allLines[[numLines]]
			if (length(vectPoints$y) > 2) {
				last2Pts <<- lapply( vectPoints[1:2], function(vect) vect[ -1: -(length(vect)-2) ] )
				lines(last2Pts, type="b", col=vectPoints$color, lwd = 2)
				lastSegDist = dist(last2Pts)
				
				allLines[[numLines]]$dist <<- c(allLines[[numLines]]$dist, lastSegDist)
				allLines[[numLines]]$color <<- c(allLines[[numLines]]$color , allLines[[numLines]]$color[1])
				
# 				totalDistances = c(totalDistances, lastSegDist)
				print( paste( c("segment number", length(allLines[[numLines]]$dist)), collapse=" ")) 
	# 			print( totalDistances )
				print( paste( c("distance last segment =", lastSegDist *scale ), collapse=" "))
				lapply(1:length(allLines), function(lineNum) print( paste( c("Total distance for line #", lineNum, sum(allLines[[lineNum]]$dist) *scale ), collapse=" ")))
# 				print( paste( c("Total distance =", sum(allLines[[numLines]]$dist) ), collapse=" "))
				print("------------------")
			}
		} else {
			cat("\n","Enter a command","\n") # prompt
			cmd <-scan(what="character", nmax=1) 
			
			if (cmd == "as") {
				# add segment
# 				vectPoints <- locator(2)
# 				lines(vectPoints, type = "b")
# 				
				numLines = length(allLines)
				vectPoints = list()
				vectPoints$y = c(0)
				vectPoints$x = c(0)
				while(1) {
					c = sample(colors(),1)
					r = col2rgb(c)
					if (mean(r) < 135) {break}
				}
				vectPoints$color = c
				
				# if the last line does not actually have values (they were all
				# deleted), then we simply reuse that instead of adding a new
				# element to allLines
				if(length( allLines[[numLines]][[1]] ) == 1) {
					allLines[[numLines]] <<- vectPoints
				} else {
					allLines[[numLines+1]] <<- vectPoints
				}
			}
			else if (cmd == "dls") {
				# delete last segment
# 				vectPoints = lapply( vectPoints, function(vect) vect[ -(length(vect)) ] )
# 				totalDistances = totalDistances[-length(totalDistances)]
# 				allLines[[numLines]]

				# the 1st check is if numLines exists. This will fail when we 
				# immediately start the program and type "dls" without doing 
				# "as" yet
				if (exists("numLines")) { 
					if (length( allLines[[numLines]][[1]] ) > 3) {
						allLines[[numLines]] <<- lapply( allLines[[numLines]], function(vect) vect[ -(length(vect)) ] )
						redrawAll(allLines)
					} else if (length( allLines[[numLines]][[1]] ) != 1){
						# There are only 2 points left, the root point and the 2nd point.
						# By deleting this segment, we are effectively deleting the entire line
						# We assign it back to the 0,0 vector and reset the distance
						# to be 0
						allLines[[numLines]] <<- lapply( allLines[[numLines]], function(vect) vect[ 1 ] )
						allLines[[numLines]]$dist <<- allLines[[numLines]]$dist[-1]
						redrawAll(allLines)				
					} else {
						print( "Nothing to delete" )
					}
				} else {
					print( "Nothing to delete" )
				}
			}
			else if (cmd == "gs") {
				# get scale
				twoPts <- locator(2)
# 				diffed = sapply(twoPts, diff)
# 				sqrted = sqrt( diff(diffed^2) )
				sqrted = dist(twoPts)
				lines(twoPts, type="o", col="red")
				cat("\n","Enter the distance you just clicked on","\n") # prompt
				distance <-scan(nmax=1) 
				scale = distance / sqrted
# 				print( paste( sqrted, distance ) )
# 				print(scale)
				lapply(1:length(allLines), function(lineNum) print( paste( c("Total distance for segment #", lineNum, sum(allLines[[lineNum]]$dist) *scale ), collapse=" ")))
# 				
			}
			else if (cmd == "q") {
				# quit
				return()
			}
			else {
				print( "Not a valid command" )
			}
			
		}
# 		print(vectPoints)
	}
	
# 	while(1) {
# 		twoPts = locator(3)
# 		print(twoPts)
# 		lines(twoPts, type="b")
# 	}
	(img)
}

redrawAll = function(allLines1) {
	plot(img, xlab="enter command")
	lapply(allLines1, function(line) lines( cbind(line[[2]][-1], line[[1]][-1]) , type="b", col=line$color, lwd = 2))
# 	last2Pts <<- lapply( vectPoints[1:2], function(vect) vect[ -1: -(length(vect)-2) ] )
}

printAllDistances = function(allLines1) {
	
}
	

dist <- function(twoPts) {
# 	print(paste(twoPts))
	diffed = sapply(twoPts, diff)
# 	print(paste(diffed))
	sqrted = sqrt( sum( diffed^2 ))
# 	print(paste(sqrted))
	return(sqrted)
# 	lines(twoPts, type="o", col="red")
	
}

img = mydist("image2.pnm")
