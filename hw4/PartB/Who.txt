Gabriel Reyla - Implementation of main function and design of architecture for the solution. Spec verification, code organizzation and readability, early QA testing. No-for-loop design scheme. Bug resolutions.
Edmund Yan - 996049162 - QA Testing, adding/deleting segments, distance function
Jack Soe - Image conversion, Skeleton design, Peer program
David Nguyen - Debugging, Peer program
Ken Nickles - Pixmap installation, Skeleton design, Peer program
